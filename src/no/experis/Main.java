package no.experis;

import java.sql.*;
import java.text.SimpleDateFormat;

public class Main {

    public static void createNewDatabase(String fileName) {
        String url = "jdbc:sqlite:Resources/" + fileName;

        try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("A new database has been created.");
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void createTables (Connection conn) throws SQLException {
        String personSql ="CREATE TABLE if not exists Person" +
                "(Id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "Gender VARCHAR(255)," +
                "FirstName VARCHAR(255)," +
                "LastName VARCHAR(255)," +
                "DOB VARCHAR(255)," +
                "Address VARCHAR(255)," +
                "ParentID1 INTEGER, "+
                "ParentID2 INTEGER);";

        String emailAddressSql = "CREATE TABLE if not exists Email" +
                "(Id INTEGER," +
                "Email VARCHAR(255)," +
                "Label VARCHAR(255)," +
                "FOREIGN KEY(Id) REFERENCES Person(Id));";

        String contactNumberSql = "CREATE TABLE if not exists Number" +
                "(Id INTEGER," +
                "Number VARCHAR(255)," +
                "Label VARVHAR(255)," +
                "FOREIGN KEY(Id) REFERENCES Person(Id));";


        Statement stmt = conn.createStatement();
        stmt.execute(personSql);
        stmt.execute(emailAddressSql);
        stmt.execute(contactNumberSql);

    }

    public static void dropTables(Connection conn) throws SQLException {
        Statement stmt = conn.createStatement();
        stmt.execute("DROP TABLE Person");
        stmt.execute("DROP TABLE Email");
        stmt.execute("DROP TABLE Number");
    }

    public static void main(String[] args) throws SQLException {

        Connection conn = connect();
        createTables(conn);



        System.out.println(insert("'male'", "'Nicolas'", "'Saint'", "'13/06/1993'","'NorthPole69'", null, null, conn));
        System.out.println(insert("'female'", "'Nicoline'", "'Saint'", "'13/06/1990'","'NorthPole69'", null, null, conn));
        System.out.println(insert("'male'", "'Nicolas jr.'", "'Saint'", "'13/06/1998'","'NorthPole69'", 1,2, conn));
        System.out.println(insert("'male'", "'Nicoline jr.'", "'Saint'", "'13/06/1998'","'NorthPole69'", 1,2, conn));


        select(conn);

        //clears all tables
        dropTables(conn);

    }


    public static boolean select(Connection conn) throws SQLException {
        boolean success = false;
        String selectPerson="SELECT * FROM Person";
        PreparedStatement preparedStatement = conn.prepareStatement(selectPerson);

        try{
           Statement stmt = conn.createStatement();
           ResultSet rs = stmt.executeQuery(selectPerson);
           while(rs.next()){
               System.out.println(rs.getString("Id") + "    " + rs.getString("FirstName") + " " + rs.getString("ParentID1")  + "   " + rs.getString("ParentID2"));
           }
        }catch(Exception e){
            e.printStackTrace();
        }


        return success;
    }

    public static void delete(Connection conn){
        String updateRow = "DELETE FROM Person WHERE Id = 2";

        try{
            Statement stmt = conn.createStatement();
            stmt.executeUpdate(updateRow);
        }catch(Exception e){
            e.printStackTrace();
        }
    }



    public static boolean insert(String gender, String firstName, String lastName, String DOB, String address, Integer parentID1, Integer parentID2, Connection conn) throws SQLException{
        boolean success = true;
        String insertintoPerson = "INSERT INTO Person (Gender, FirstName, LastName, DOB, Address, ParentID1, ParentID2) "+
                                  "VALUES (" + gender + ", "+ firstName + ", " + lastName + ", " + DOB +", " +  address +","+parentID1 +","+parentID2+");";
        System.out.println(insertintoPerson);
        PreparedStatement preparedStatement = conn.prepareStatement(insertintoPerson);


        boolean autoCommit = conn.getAutoCommit();
        try{
            conn.setAutoCommit(false);
            preparedStatement.executeUpdate();
            conn.commit();
        }catch (SQLException e){
            success=false;
            conn.rollback();
            e.printStackTrace();
        }finally {
            conn.setAutoCommit(autoCommit);
        }
        return success;
    }

    public static Connection connect() {
        Connection conn = null;
        try {
            String url = "jdbc:sqlite:Resources/ContactList.db";
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been established.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }
}